/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jogo.cg.modulo2;

import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.gl2.GLUT;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Random;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLJPanel;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;

/**
 *
 * @author Lucas Gaspar
 */
public class Game implements GLEventListener,
        KeyListener {

    public static void main(String args[]) {
        new Game();
    }

    GLU glu = new GLU();
    GLUT glut = new GLUT();

    int dir = 1;
    float maxHeight = 1.25f;
    float loseHeight = 0f;
    float sphereHeight = 0;
    float gravitySpeed = 0.01f;
    float gravitySpeedO = 0.01f;
    float sphereX = 0f;
    float sphereSpeed = 0.005f;

    float moveLimit = 4f;
    float distanceResp = -9.75f;
    int maxInstances = 11;
    float moveSpeed = 0.005f;
    float moveSpeedO = 0.005f;
    float instances[];
    float minX = -.75f;
    float maxX = .75f;
    float instancesX[];
    float size = 0.3f;
    int index = 0;

    float speedBase = 1.25f;
    private double g;

    private float RandomX() {
        Random r = new Random();
        float f = (r.nextFloat() * (maxX - minX) + minX) * 1.0f;
        return 0;
    }

    public Game() {
        instances = new float[maxInstances];
        instancesX = new float[maxInstances];
        index = 0;
        for (int i = 0; i < maxInstances; i++) {
            if (i != 0) {
                instancesX[i] = RandomX();
                instances[i] = -(i * 1.25f + maxHeight);
            } else {
                instancesX[i] = 0;
                instances[i] = -(i + maxHeight);
            }
        }

        updateSpeed(speedBase);

        GLJPanel canvas = new GLJPanel();
        canvas.addGLEventListener(this);

        JFrame frame = new JFrame("Game");
        frame.setSize(500, 500);
        frame.getContentPane().add(canvas);
        frame.setVisible(true);
        frame.addKeyListener(this);

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                new Thread(new Runnable() {
                    public void run() {
                        System.exit(0);
                    }
                }).start();
            }
        });

    }

    private void updateSpeed(float base) {
        speedBase = base;
        gravitySpeed = gravitySpeedO * base;
        moveSpeed = gravitySpeed * moveSpeedO / gravitySpeedO;
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();

        Animator ani = new Animator(drawable);
        ani.start();

        glut = new GLUT();

        //Habilita o teste de profundidade
        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glEnable(GL2.GL_LIGHT1);

        gl.glRotatef(45, 1, 0, 0);

        float luzDifusa[] = {1f, 1f, 1f, .5f};

        gl.glLightfv(GL2.GL_LIGHT1,
                GL2.GL_DIFFUSE,
                luzDifusa, 0);

        float position[] = {0f, 3f, 2f};

        gl.glLightfv(GL2.GL_LIGHT1,
                GL2.GL_POSITION,
                position, 0);

        float luzAmbiente[] = {1f, 1f, 1f, 1.0f};
        gl.glLightfv(GL2.GL_LIGHT1,
                GL2.GL_AMBIENT,
                luzAmbiente, 0);
    }

    @Override
    public void display(GLAutoDrawable glAuto) {

        GL2 gl = glAuto.getGL().getGL2();
        gl.glClear(GL.GL_COLOR_BUFFER_BIT
                | GL.GL_DEPTH_BUFFER_BIT
        );

        gl.glLoadIdentity();
        gl.glRotated(90, 0, 1, 0);

        sphereHeight += gravitySpeed * dir;

        if (sphereHeight >= maxHeight) {
            sphereHeight = maxHeight;
            dir = -1;
        } else if (sphereHeight <= loseHeight) {
            Jump();
        }
        
        if(right){
            sphereX += sphereSpeed;
        }
        else if(left){
            sphereX -= sphereSpeed;
        }
        
        gl.glPushMatrix();
        float matDifusaSphere[] = {1f, 1f, 0f, 0.0f};
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK,
                GL2.GL_DIFFUSE,
                matDifusaSphere,
                0);
        gl.glTranslatef(sphereX, sphereHeight, 0);
        gl.glScalef(0.10f, 0.10f, 0.10f);
       // gl.glRotatef(90, 1, 0, 0);
        glut.glutSolidSphere(1, 25, 25);
        gl.glPopMatrix();

        float matDifusa[] = {1f, 0f, 0f, 0.0f};

        for (int i = 0; i < maxInstances; i++) {

            float pos = instances[i];

            gl.glPushMatrix();
            gl.glMaterialfv(GL.GL_FRONT_AND_BACK,
                    GL2.GL_DIFFUSE,
                    matDifusa,
                    0);
            gl.glTranslatef(instancesX[i], 0, pos);
            gl.glScalef(size, size, size);
            gl.glRotatef(90, 1, 0, 0);
            glut.glutSolidCylinder(1, 0.5, 25, 25);
            gl.glPopMatrix();

            pos += moveSpeed;

            if (pos > moveLimit) {
                pos = distanceResp;
                instancesX[i] = RandomX();
            }

            instances[i] = pos;
        }

    }

    private void Jump() {

        if (SphereCollidWithBase()) {
            dir = 1;
            sphereHeight = 0;
            index++;
            if (index >= maxInstances) {
                index = 0;
            }
        } else {
            gravitySpeed = 0;
            //LOSE GAME!
        }

    }

    private boolean SphereCollidWithBase() {
        float baseX = instancesX[index];

        if (sphereX >= baseX + size) {
            return false;
        }

        if (sphereX <= baseX - size) {
            return false;
        }
        
        return true;
    }

    @Override
    public void dispose(GLAutoDrawable glad) {

    }

    private boolean left;
    private boolean right;

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            left = true;
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            right = true;
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            left = false;
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            right = false;
        }

    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int w, int h) {
        GL2 gl = drawable.getGL().getGL2();

        gl.glViewport(0, 0, w, h);
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        if (w <= h) {
            gl.glOrtho(-1.5, 1.5,
                    -1.5 * (float) h / (float) w,//
                    1.5 * (float) h / (float) w,//
                    -10.0, 10.0
            );
        } else {
            gl.glOrtho(-1.5 * (float) w / (float) h, //
                    1.5 * (float) w / (float) h, //
                    -1.5, 1.5, -10.0, 10.0
            );
        }

        gl.glRotatef(13, 1, 0, 0);
        gl.glTranslatef(0, -1, -2);

        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

}
